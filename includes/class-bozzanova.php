<?php
/**
* @package   Bozzanova Support 2021
* @author    Jon Täng <jon@bozzanova.se>
* @link      http://www.bozzanova.se
* @copyright 2021 Bozzanova AB
*/

namespace NS_SUPPORT;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Bozzanova Class
 */
class Bozzanova {
	
	public $site = '';
	public $developer = '';
	public $website =  '';
	public $facebook = '<a href="https://www.facebook.com/bozzanova.se/" target="_blank">Facebook</a>';
	public $linkedin = '<a href="http://www.linkedin.com/company/bozzanova-ab" target="_blank">LinkedIn</a>';
	public $twitter = '<a href="https://twitter.com/bozzanova" target="_blank">Twitter</a>';
	public $instagram = '<a href="https://www.instagram.com/bozzanova.se/" target="_blank">Instagram</a>';
	
	private static $instance = null;
	
	/**
	* Instance function
	*
	* @return Theme
	*/
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * Class Constructor
	 */
	function __construct() {
		add_action( 'init', [ $this, 'set_site' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_styles_scripts' ] );
		//$this->set_site();
		// Branding
		add_filter( 'admin_footer_text', [ $this, 'admin_footer' ]);
		add_action( 'wp_dashboard_setup', [ $this, 'dashboard_widgets' ] );
		
		// Support Page
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_action( 'admin_menu', [ $this, 'admin_submenu' ] );

		add_action( 'admin_bar_menu', [ $this, 'customize_toolbar' ] );
	}

	public function admin_styles_scripts() {
		$screen = get_current_screen(); 
		// global $pagenow;
		// echo '<pre style="margin: 100px 0 0 400px">'; print_r($screen); echo '</pre>';

		wp_register_style( 'bozzanova-support-style', NS_SUPPORT_URL . 'assets/css/style.css', false, null );
        
		if ( 'dashboard_page_bozzanova-support' == $screen->base ) {
			wp_enqueue_style( 'bozzanova-support-style' );
		}

	}
	
	/**
	 * Set site information
	 */
	public function set_site() {
		$site = get_site_url(); 
		$site = str_replace('https://', '', $site);
		$site = str_replace('http://', '', $site);
		
		$this->site = $site;
		$this->developer = sprintf( 
			'<a href="https://www.bozzanova.se/?utm_source=customer_site&utm_medium=website&utm_campaign=support&utm_content=%s" target="_blank">%s</a>', 
			$site, 
			__( 'Bozzanova Agency', 'nova-support' ) 
		);
		$this->website = sprintf( 
			'<a href="https://www.bozzanova.se/?utm_source=customer_site&utm_medium=website&utm_campaign=support&utm_content=%s" target="_blank">%s</a>', 
			$site, 
			__( 'Website', 'nova-support' ) 
		);
	}
	
	//--------------------------------------------------------------------------------
	//	Bozzanova THEME Brading	
	//--------------------------------------------------------------------------------
	
	/*
	* Custom Backend Footer
	*/
	public function admin_footer() {
		printf( 
			'<span id="footer-thank-you">%s</span>',
			sprintf( 
				'%s %s',
				__( 'Developed by', 'nova-support' ),
				$this->developer
			)
		);
	}

	/**
	 * Add link in WP Admin Toolbar
	 * 
	 * @param array $wp_admin_bar
	 * @return array $wp_admin_bar
	 */
	public function customize_toolbar( $wp_admin_bar ) {
		// echo '<pre>'; print_r( $wp_admin_bar ); echo '</pre>';
		$wp_admin_bar->add_node( [
			'id'		=> 'bozzanova-support',
			'title'		=> sprintf('<span class="ab-icon"></span><span class="ab-label">%s</span>', __( 'Bozzanova Support',  'nova-support' ) ),
			// 'href'		=> admin_url( 'index.php?page=bozzanova-support' ),
			'href'		=> admin_url( 'admin.php?page=bozzanova-support' ),
			'parent'    => 'top-secondary'
     ] );
	}
	
	/**
	* Create the function to output the contents of our Dashboard Widget.
	*/
	public function dashboard_widget() {
		// Display whatever it is you want to show.
		$current_user = wp_get_current_user();
		printf( '<div><strong>%s %s</strong><br>%s</div>', 
			__('Hello', 'nova-support'), 
			($current_user->firsname_name !== '') ? $current_user->firsname_name: $current_user->display_name, 
			__( 'Thanks for using a product or service from Bozzanova Agency.', 'nova-support' ) 
		);
	
		echo '<div style="border-bottom: 1px solid #eee;margin: 0 -12px;padding: 8px 12px 4px;">';

		printf( '<h2>%s</h2>', __('Need support?', 'nova-support' ) );
		
		printf( 
			'<div>%s<br>%s: %s<br>%s: %s</div>', 
			__('You are welcome to contact us if you need any kind of help.', 'nova-support'),
			__('Call', 'nova-support' ),
			'0521 - 186 20',
			__('Email', 'nova-support' ),
			'<a href="mailto:support@bozzanova.se">support@bozzanova.se</a>'
		);

		printf(
			'<div style="margin: .5rem 0;">%s %s %s</div>', 
			__('Visit our', 'nova-support' ),
			$this->website,
			__('for more information about our services.', 'nova-support' )
		);

		printf(
			'<div style="margin: .5rem 0;">%s: %s, %s, %s, %s</div>',
			__('Follow Bozzanova on', 'nova-support'),
			$this->facebook,
			$this->linkedin,
			$this->twitter,
			$this->instagram
		);

		printf( '<div style="margin: .5rem 0;">%s</div>',
			sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=bozzanova-support' ), __('For more support information', 'nova-support') )
		);
		echo '</div>';
	}

	/**
	* Add a widget to the dashboard.
	*
	* This function is hooked into the 'wp_dashboard_setup' action below.
	*/
	public function dashboard_widgets() {
		wp_add_dashboard_widget(
			'bozzanova_dashboard_widget', // Widget slug.
			__('Bozzanova Agency', 'nova-support' ), // Title.
			[ $this, 'dashboard_widget' ] // Display function.
		);	
	}

	//--------------------------------------------------------------------------------
	//	Bozzanova THEME Support Page	
	//--------------------------------------------------------------------------------
	/** 
	* Add Support Page
	*/
	function admin_menu() {

		// echo '<pre style="margin-ßleft: 250px">'; print_r( $GLOBALS['admin_page_hooks'] ); echo '</pre>';
		
		//empty ( $GLOBALS['admin_page_hooks']['my_unique_slug'] ) )

		add_dashboard_page(
			__('Bozzanova Support', 'nova-support' ),
			__('Support', 'nova-support' ),
			'read',
			'bozzanova-support',
			[ $this, 'render_page' ]
		);

		add_menu_page(
			__('Bozzanova', 'nova-support' ),
			__('Bozzanova', 'nova-support' ),
			'edit_posts',
			'bozzanova',
			[ $this, 'render_page' ],
			'dashicons-admin-settings',
			90
		);
	}
		
	function admin_submenu() {
		add_submenu_page(
			'bozzanova',
			__('Bozzanova Support', 'nova-support' ),
			__('Support', 'nova-support' ),
			'edit_posts',
			'bozzanova-support',
			[ $this, 'render_page' ],
			90
		);

		remove_submenu_page( 'bozzanova', 'bozzanova');
	}

	/** 
	* Render Support Page
	*/
	function render_page() {
		$site = get_site_url(); 
		$site = str_replace('https://', '', $site);
		$site = str_replace('http://', '', $site);
		//get_template_part('parts/admin/support');
		?>
		
		<div class="wrap novastyle">
		<h1><span class="icon"><svg aria-hidden="true" focusable="false" data-icon="bozzanova" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.46 283.46"><rect fill="currentColor" y="261.66" width="239.17" height="21.8"/><rect fill="currentColor" y="218.05" width="283.46" height="21.8"/><rect fill="currentColor" y="174.44" width="283.46" height="21.8"/><rect fill="currentColor" y="130.83" width="256.89" height="21.8"/><rect fill="currentColor" y="87.22" width="283.46" height="21.8"/><rect fill="currentColor" y="43.61" width="283.46" height="21.8"/><rect fill="currentColor" width="239.17" height="21.8"/></svg></span>Bozzanova Support</h1>
		
		<div class="poststuff">
		
		<div class="row">
		
		<div class="col left">
		
		<div class="postbox">
		<div class="inside">
		<h2>Behöver ni support?</h2>
		<p>Ni är välkomna att kontakta oss om ni behöver hjälp med hemsidan, e-post eller på något annat vis.</p>
		<p>Ring oss på: <strong>0521 - 186 20</strong> eller skicka e-post till <a href="mailto:support@bozzanova.se?subject=Vi behöver hjälp med <?php echo $site; ?>"><strong>support@bozzanova.se</strong></a></p>       
		<p><strong>OBS!</strong><br> Innan ni ringer eller mejlar om problem med hemsidan är det bra att ta skärmbild på felet/problemet och noterar vilken länk på hemsidan det gäller. Då blir det lättare för oss att felsöka eller svara på de frågor ni har.</p>
		
		<h3>Support-verktyg</h3>
		<p>Ladda ner <a href="https://www.teamviewer.com/sv/teamviewers-automatiska-nedladdning/" target="_blank">Teamviewer</a> så kan vi enklare ge er support på distans med skärmdelning och fjärrstyrning.</p>
		
		<h3>Mer om Bozzanova</h3>
		<p>Bozzanova är en reklambyrå som kan hjälpa er med all form av marknadsföring och kommunikation.</p>
		<p>För mer information om våra tjänster: <strong><?php echo $this->website; ?></strong>. Följ oss gärna på våra sociala kanaler: <strong><?php echo $this->facebook; ?></strong>, <strong><?php echo $this->linkedin; ?></strong>, <strong><?php echo $this->twitter; ?></strong> och <strong><?php echo $this->instagram; ?></strong>.</p>
		
		<h3>Behöver ni ett supportavtal?</h3>
		<p>Bozzanova har ett förmånligt avtal där Bozzanova tar hand om både hemsidan och dess användare. Bozzanova sköter om uppdateringar och ser till att hemsidan är online. De som arbetar med hemsidan kan få prioriterad hjälp via telefon och e-post.</p>
		<p>Ring oss på: <strong>0521 - 186 20</strong> eller skicka e-post till <a href="mailto:info@bozznova.se?subject=Vi behöver ett supportavtal till <?php echo $site; ?>"><strong>info@bozznova.se</strong></a> så får ni mer informaiton.</p>
		</div>
		</div>
		
		
		
		</div> <!-- .col.left -->
		
		<div class="col right">
		<div class="postbox">
		<div class="inside">
		<h2>Vardagstips från Bozzanova</h2>
		
		<h3>Filer och Media</h3>
		<p><strong>Innan du laddar upp</strong> filer i Mediabiblioteket döper du om filen. Använd upp till 5 ord som beskriver bildens innehåll (tänk sökord) + ditt varumärke. Använd inte ÅÄÖ eller konstiga tecken (t.ex. #¤%&/) i filnamnet. Separera orden med bindestreck (-) istället för mellanslag.</p>
		<p>Ex: <em>bat-hav-sol-bozzanova-reklambyra.jpg</em></p>
		<p><strong>Efter du lattat upp</strong> filer i Mediabiblioteket skriver du en ordentlig beskrivning (60-70 tecken) av filens innehåll i Rubrikfältet. Här får du använda alla typer av tecken. Glöm inte att använda sökord som hör till sidan som bilden ska finnas med på. Detta hjälper till i bl.a. sökoptimeringen och ökar tillgängligheten på hemsidan.</p>
		<p>Ex: <em>En liten båt ute på havet i mysigt solljus.</em></p>
		
		<h4>Bildstorlekar</h4>
		<p>Även om WordPress skapar en mängd olika bildstorlekar (beroende på tema eller egna inställningar) när man laddar upp bilder så är det bra om man kan försöka att anpassa bildens storlek till det ändamål den är tänkt för. T.ex. om en bild ska användas som en s.k. Hero-bild (en stor bild som sträcker sig över hela skärmens bredd) bör den minst vara 1920 pixlar bred.</p>
		
		<h4>Video</h4>
		<p>Det är idag viktigt att använda video i alla kanaler. Använd tjänster som t.ex. Youtube eller Vimeo för att presentera video på hemsidan. Deras servrar är anpassade för videostreaming vilket de flesta vanliga webbservrar inte är.</p>
		
		<h3>Innehållet</h3>
		<p>Varje sida ska bära sig själv - d.v.s. handla om ETT ämne. Detta ämne är den sökfras (som inkluderar böjningar och synonymer) som man ska optimera sidan mot. Gör så att innehållet passar för den tänkta målgruppen. Växla av långa texter med bilder. Baka in video-filmer i innehållet. Skriv lättsam säljande text först. När texten är klar börjar du anpassa texten med sökord och fraser. Ett sidinnehåll bör innehålla minst 300 ord. Se till att varje sida har en s.k. Call to Action (CTA) som uppfyller något av verksamhetsmålen. T.ex. boka ett möte.</p>
		<p>Enligt Google: <em>Ett användarvänligt innehåll som svarar på besökarens sökning är detsamma som ett sökoptimerat innehåll.</em></p>
		
		<h4>Viktiga sidor</h4>
		<p>Integritetspolicy - behövs enligt EU:s lagstiftning.<br>
		Information om kakor - behövs enligt EU:s lagstiftning.<br>
		Köp och leverensvillkor - kan behövas om betalfunktioner används på hemsidan (t.ex. vid webshop)</p>
		</div>
		</div>
		
		<?php /*
		<div class="postbox">
		<div class="inside">
		<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fbozzanova.se%2F&tabs=timeline&width=500&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=261115241029502" width="100%" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
			</div>
			</div>
			*/ ?>
			
			</div> <!-- .col.right -->
			
			</div> <!-- .row -->
			
			</div>
			</div>
		<?php
	}
}