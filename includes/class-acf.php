<?php
/**
* @package   Bozzanova Support 2021
* @author    Jon Täng <jon@bozzanova.se>
* @link      http://www.bozzanova.se
* @copyright 2021 Bozzanova AB
*/

namespace NS_SUPPORT;

/**
 * ACF Class
 */
class ACF {

    /**
     * Class Constructor
     */
    public function __construct() {

        add_filter( 'acf/settings/save_json', [ $this, 'json_save_point' ] );
        add_filter( 'acf/settings/load_json', [ $this, 'json_load_point' ] );
    }

    /**
     * ACF JSON Save Point
     *
     * @param Array $paths
     * @return Array $paths
     */
    public function json_save_point( $path ) {
        
        // update path
        $path = NS_SUPPORT_PATH . '/assets/acf-json';
        
        // return
        return $path;
        
    }
    
    /**
     * ACF JSON Load Point
     * 
     * @param Array $paths
     * @return Array $paths 
     */
    public function json_load_point( $paths ) {
        
        // remove original path (optional)
        unset($paths[0]);
        
        // append path
        $paths[] = NS_SUPPORT_PATH . '/assets/acf-json';
        
        // return
        return $paths;
        
    }
}