<?php
/**
 * Plugin Name:       Bozzanova Support
 * Plugin URI:        https://www.bozzanova.se/
 * Description:       Handle Cookies and Tracking.
 * Version:           1.0
 * Requires at least: 5.5
 * Requires PHP:      7.2
 * Author:            Bozzanova Team <info@bozzanova.se>
 * Author URI:        https://www.bozzanova.se/
 * License:           Copyright Bozzanova AB
 * License URI:       https://www.bozzanova.se/
 * Text Domain:       nova-support
 * Domain Path:       /languages
 */

namespace NS_SUPPORT;

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('Support') ) :

    class Support {

        /** @var string The plugin version number. */
        var $version = '1.0';
        
        /** @var array Storage for class instances. */
        public $ns_support = [];

        public function __construct() {
            // do nothing
        }

        function initialize() { 
            // Define constants.
            $this->define( 'NS_SUPPORT', true );
            $this->define( 'NS_SUPPORT_PATH', plugin_dir_path( __FILE__ ) );
            $this->define( 'NS_SUPPORT_URL', plugin_dir_url( __FILE__ ) );
            $this->define( 'NS_SUPPORT_BASENAME', plugin_basename( __FILE__ ) );
            $this->define( 'NS_SUPPORT_VERSION', $this->version );
            $this->define( 'NS_SUPPORT_MAJOR_VERSION', 5 );

            $this->includes();

            add_action( 'init', array($this, 'init'), 5 );

            $this->ns_support[] = new Bozzanova();
            $this->ns_support[] = new ACF();

            // if( ! function_exists('get_plugin_data') ){
            //     require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            // }
            // echo '<pre>'; print_r( get_plugin_data( NS_SUPPORT_PATH . 'bozzanova-support.php' ) ); echo '</pre>'; 

            // $this->check_update();

        }

        public function init() {
            load_plugin_textdomain( 'nova-support', false, 'bozzanova-support/languages' );
        }

        function includes() {
            include_once( NS_SUPPORT_PATH . '/includes/class-bozzanova.php' );
            include_once( NS_SUPPORT_PATH . '/includes/class-acf.php' );
        }

        function define( $name, $value = true ) {
            if( !defined($name) ) {
                define( $name, $value );
            }
        }

        function check_update() {
            include_once( NS_SUPPORT_PATH . '/includes/class-bb-updater.php' );

            $bb_plugin = array(
                'plugin_file' => 'bozzanova-support.php',
                'plugin_slug' => 'bozzanova-support',
                'bb_host' => 'https://api.bitbucket.org',
                'bb_download_host' => 'http://bitbucket.org',
                'bb_owner' => '',
                'bb_password' => '',
                'bb_project_name' => 'teambozzanova',
                'bb_repo_name' => 'bozzanova-support'
            );

            // echo '<pre>', print_r( get_plugin_data( NS_SUPPORT_PATH . 'bozzanova-support.php' ) ); echo '</pre>'; 
    
            new Bitbucket_Updater( $bb_plugin );

            // GIT UPDDATER
            // if ( (string) get_option( 'bozzanova_support_license' ) !== '' ) {
            //     include_once( NS_SUPPORT_PATH . '/includes/class-updater.php';
            
            //     $updater = new PDUpdater(__FILE__);
            //     $updater->set_username( 'bozzanova' );
            //     $updater->set_repository( 'bozzanova-support' );
            //     $updater->authorize( get_option( 'bozzanova_support_license' ) );
            //     $updater->initialize();
            // }
        }
        
    }

    function Nova_Support() {
        global $ns_support;

        $ns_support = new Support();
        $ns_support->initialize();

        return $ns_support;
    }
    Nova_Support();

endif;